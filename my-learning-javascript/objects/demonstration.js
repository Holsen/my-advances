// How to define an object?
// - Well, there are two ways to define it.

// FIRS FORM
const newObj = {
  key: 'value'
}
console.log('first form:', newObj);

// SECOND FORM
let cat = new Object();
cat['color'] = 'white with brown';
cat['eyes color'] = 'sky-blue';
console.log(`second form: ${JSON.stringify(cat)}`);

