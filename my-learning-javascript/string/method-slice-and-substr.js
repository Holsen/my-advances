
const str = 'Hi, My name is Holsen, What\'s your name?';
console.log(str);

/* method - slice()
 * extracts a sub-string or section of a string.
 * return a string that contains the extracted portion of string.
 * the first caracter has the position 0, the second 1, and so on.
 * IMPORTANT : does not modify the original string.
 * TIP : Use a negative number to select from the end of the string.
 */

// syntax : string.slice(start, end)
console.log(str.slice(0, 5));

/* method substr()
 * return a portion of the string between the start and end indexes or to the end of the string.
 * IMPORTANT : the method does not change the origin string.
 * TIP : To extract characters from the end of the string, use a negative start number.
 */

// syntax : string.substr(start, length);
console.log(str.substr(0, 5));


/* What is the difference between slice and substr? */
// there 2 functions are quite similar in their 'syntax'
