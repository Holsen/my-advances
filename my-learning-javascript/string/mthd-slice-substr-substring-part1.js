// DIFERENCIA ENTRE SLICE, SUBSTR Y SUBSTRING

let lenguages = 'Python, JavaScript and Go';

// SLICE()
// LOS INDICES DE EXTRACCIÓN PUEDEN SER NEGATIVOS
// EL EXTREMO DERECHO O LIMITE SUPERIOR "->" NO SE INCLUYE, TIENE QUE SER +1

// indices positivos
let resultado = lenguages.slice(8, 18);
console.log(resultado);

// indices negativos
resultado = lenguages.slice(-19, -9);
console.log(resultado);

// SUBSTRING()
// METODO DE INSTANCIA DE LOS METODOS STRING
// NO PERMITE INDICES NEGATIVOS

resultado = lenguages.substring(8, 18);
console.log(resultado);

// SUBSTR()
// PODEMOS INDICAR EL PRIMIR PARAMETRO INDICE, DONDE QUEREMOS ESPERAR LA EXTRACCIÓN Y EL SEGUNDO EL LA LONGITUD.
resultado = lenguages.substr(8, 10);
console.log(resultado);
