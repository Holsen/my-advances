// lo aprendido

let cadena = 'My name is Code, I\'m from Perú';

// method - slice(start, end)
for (let i = 1; i <= cadena.length; i++) {
//  console.log(`*-${cadena.slice(0, i)}-*`);
}

const cadenaLength = cadena.length;
const cadenaInOf = cadena.indexOf('ú');
console.log('* Logitud del string es :', cadenaLength);
console.log(cadena.slice(0, cadenaLength));
console.log(cadena.slice(0, cadenaInOf + 1));

console.log(cadena.slice(11, 15));
console.log(cadena.slice(-19, -15));

/* METHOD - substring(); */
console.log(cadena.substring(11, 15));
console.log(`<|${cadena.substring(15, 11)}|>`);
// <[No Admite negativos]> console.log(cadena.substring(-19, -15));

/* METHOD - substr() */
// conclusion, las parametros, numero de inicio y recorrido
console.log(cadena.substr(11, 15));
let contador = 0;
for (const i of cadena.substr(11, 15)) {
  console.log(i, contador++ + "[" + contador + "]");
}
// si queremos imprimir 'Code' seria del 11, 4;
console.log("method - substr(11, 4) =", cadena.substr(11, 4));
