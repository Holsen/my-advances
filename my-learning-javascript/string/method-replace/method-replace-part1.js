/* METHOD - replace()
 * [Please note] : the method doesn't change the 'origin string'. It returns a new String.
 * replace the first occurrence of the substring with the new string to replace.
 * syntax : str.replace(regexp/substr, newSubstr); */

let string = 'I am programming in Python, Python is very interesting';
// <[checked : It does'nt change the origin string]>
// newString.replace('Python', 'JavaScript');
console.log(string);

let newString = string.replace('Python', 'JavaScript');
console.log(newString); // opt : I... JavaScript, Python is...

// USING REGULAR EXPRESSION (Regexp)
// syntax : str.replace(regexp, newsubstr);

/* in this syntax, the replace() method find all matches in the str, replaces them by the new-substr, and return a new string(new-str)*/ 

// the following example uses the global flag(g) to replace all ocurrence of the Python in the str by the JavaSript.

let newStr = newString.replace(/Python/g, 'JavaScript');
console.log(newStr);

// probando una cosa que se me vino a la mente.
let regularExp = /Python/g;
let newStrTwo = newString.replace(regularExp, 'Css3');
console.log(newStrTwo);
