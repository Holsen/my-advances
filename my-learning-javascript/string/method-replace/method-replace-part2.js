const cadena = "JavaScript es asombroso, Con el (JavaScript) puedes hacer maravillas";
console.log(cadena);

const resultado1 = cadena.replace('JavaScript','Python');
console.log(resultado1);

const resultado2 = cadena.replace(/JavaScript/g,'Python');
console.log(resultado2);

const expRegularG = /JavaScript/g;
const resultado3 = cadena.replace(expRegularG, 'Express');
console.log(resultado3);

const expRegularI = /Javascript/i;
const resultado4 = cadena.replace(expRegularI, 'C#');
console.log(resultado4);

const expRegularGI = /JavascripT/gi;
const resultado5 = cadena.replace(expRegularGI, 'Django');
console.log(resultado5);

// using a replacement function
// syntax : str.replace(substr | regexp, replacer);
// function syntax : function replacer(march, p1, p2, ..., offset, string);

let str = 'I like to eat, eat, eat apples and bananas';
let reg = /apples|bananas/gi;

let newStr = str.replace(reg, match => {
  console.log({match});
  return match.toUpperCase();
});

console.log(newStr);
// https://www.javascripttutorial.net/javascript-string-replace/


// replaceAll()
// Note : that the String.prototype.replaceAll() method was added in ES2021/ES12.
//const cambio = cadena.replaceAll('JavaScript','C');
//console.log(cambio); 
// opt : for now I got error because I don't have ES12.
